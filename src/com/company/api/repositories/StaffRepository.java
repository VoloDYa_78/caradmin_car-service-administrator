package com.company.api.repositories;

import com.company.entities.Master;

import java.util.ArrayList;

public interface StaffRepository {

    public ArrayList<Master> getMasters();
    public void setMasters(ArrayList<Master> masters);
}
