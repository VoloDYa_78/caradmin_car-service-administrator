package com.company.api.services;

import com.company.entities.Order;
import com.company.entities.enums.OrderStatus;

public interface OrderManagerService {

    public void AddOrderInOrderList(Order order);
    public void DeleteOrderFromOrderListById(int orderId);
    public void ChangeOrderStatus(int orderId, OrderStatus orderStatus);
    public void ShiftOrderTime(long orderId, int shiftingTime);
}
