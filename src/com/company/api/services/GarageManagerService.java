package com.company.api.services;

import com.company.entities.GarageSpace;

public interface GarageManagerService {

    public void AddSpaceToGarage(GarageSpace garageSpace);
    public void RemoveSpaceFromGarage(long spaceId);
}
