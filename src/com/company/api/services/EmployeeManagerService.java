package com.company.api.services;

import com.company.entities.Master;
import com.company.entities.Order;

public interface EmployeeManagerService {

    public void AddMasterToStaff(Master master);
    public void AddMasterToOrderFromStaff(int masterId, int orderId);
}
