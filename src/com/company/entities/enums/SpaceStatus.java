package com.company.entities.enums;

public enum SpaceStatus {
    EMPTY,
    FULL
}
