package com.company.entities.enums;

public enum OrderStatus {
    COMPLETED,
    CANCELED
}
